# NewFile-Mac

![OpenTerm Example](/Resources/New-File-Mac.gif)


## About
This is small mac application that allows a user to cerate a new file by clicking on a button in the Finder menu.  The program is a relatively simple applescript that has been compiled into Mac application that can be placed in the Finder menu bar.


## Downloads

You can download a pre-compiled binary of the Mac app via the tags page on the repo. 


*  [NewFile v1.0](https://gitlab.com/kpmoran-public/NewFile-Mac/tags)

